// JavaScript Document
function lineGraph(){
	
	// Set the margins
	var width = 500;
	var height = 300;
	var margin = 50;
	var duration = 250;
	
	var lineOpacity = "0.25";
	var lineOpacityHover = "0.85";
	var otherLinesOpacityHover = "0.1";
	var lineStroke = "1.5px";
	var lineStrokeHover = "2.5px";
	
	var circleOpacity = '0.85';
	var circleOpacityOnLineHover = "0.25"
	var circleRadius = 3;
	var circleRadiusHover = 6;
	var circleRadius_pred = 8;
	var circleRadiusHover_pred = 10;
	var svg='';
	
	
	function init(container){
		
		//var margin = 50,
   // width = window.innerWidth - margin.left - margin.right,
   // height = window.innerHeight - margin.top - margin.bottom;

   
		/* Add SVG */
		svg = d3.select(container).append("svg")
		  .attr("width", (width+margin)+"px")
		  .attr("height", (height+margin)+"px")
		  .append('g')
		  .attr("transform", `translate(${margin}, ${margin})`);
		
	}
	
	
	function render(data){
		draw(data);
	}
	
	function draw(data){
		
		/* Format Data */
		var parseDate = d3.timeParse("%Y-%m-%d %H:%M");
	//	let deviceData=data;
		data.forEach(function(d,i) { 
		console.log(d);
		console.log(data[i]);
		 
		//for(var i=0;i<deviceData.length;i++){
		 // var d=deviceData[i];
		  var tmp_graph_type = d.type;
		  var tmp_metric_type = d.name;	
		  var tmp_device_name = d.device;	
		  
		  data[i].values.forEach(function(d1) {
			//d1.orginal_date = d1.date;  
			//d1.cur_date=d1.cur_date;
			d1.graph_type = tmp_graph_type  
			d1.metric_type = tmp_metric_type;
			d1.device_name = tmp_device_name;
			var test=parseDate(d1.date);
			//console.log("*********************"+parseDate(d.date));
			d1.date = parseDate(d1.date);
			//d.date = d.date;
			if('predicted'==tmp_metric_type){
			if(undefined!=d1.pred_val){
				d1.pred_val = d1.pred_val;
			}else{
				d1.pred_val = "0";
			}
			}
			d1.metrics_val = +d1.metric_val;    
		  });
		  console.log(data);
		});
		data.forEach(function(d,i) {
			data[i].values.forEach(function(d1) {
				
			});
		});
		
		/* Scale */
		var xScale = d3.scaleTime()
		  .domain(d3.extent(data[0].values, d => d.orginal_date))
		  .range([0, width-margin]);
		
		var yScale = d3.scaleLinear()
		  .domain([d3.min(data[0].values, d => d.metrics_val), d3.max(data[0].values, d => d.metrics_val)])
		  .range([height-margin, 0]);
		
		var color = d3.scaleOrdinal(d3.schemeCategory10);
		
		
		/* Add line into SVG */
		var line = d3.line()
		  .x(d => xScale(d.orginal_date))
		  .y(d => yScale(d.metrics_val));
		
		
		var lines = svg.append('g')
		  .attr('class', 'lines');
		
		lines.selectAll('.line-group')
		  .data(data).enter()
		  .append('g')
		  .attr('class', 'line-group')  
		  .on("mouseover", function(d, i) {
			  svg.append("text")
				.attr("class", "title-text")
				.style("fill", color(i))        
				.text(d.name)
				.attr("text-anchor", "middle")
				.attr("x", (width-margin)/2)
				.attr("y", 5);
			})
		  .on("mouseout", function(d) {
			  svg.select(".title-text").remove();
			})
		  .append('path')
		  .attr('class', 'line')  
		  .attr('d', d => line(d.values))
		  .style('stroke', (d, i) => color(i))
		  .style('opacity', lineOpacity)
		  .on("mouseover", function(d) {
			  d3.selectAll('.line')
							.style('opacity', otherLinesOpacityHover);
			  d3.selectAll('.circle')
							.style('opacity', circleOpacityOnLineHover);
			  d3.select(this)
				.style('opacity', lineOpacityHover)
				.style("stroke-width", lineStrokeHover)
				.style("cursor", "pointer");
			})
		  .on("mouseout", function(d) {
			  d3.selectAll(".line")
							.style('opacity', lineOpacity);
			  d3.selectAll('.circle')
							.style('opacity', circleOpacity);
			  d3.select(this)
				.style("stroke-width", lineStroke)
				.style("cursor", "none");
			});
		
		
		/* Add circles in the line */
		lines.selectAll("circle-group")
		  .data(data).enter()
		  .append("g")
		  .style("fill", (d, i) => color(i))
		  .selectAll("circle")
		  .data(d => d.values).enter()
		  .append("g")
		  .attr("class", "circle")  
		  .attr("device_name", function(d) {
				return d.device_name;
		  })  
		  .attr("cur_time", function(d) {
				return d.cur_date;
		  })
		  .attr("graph_type", function(d) {
			  	return d.graph_type;
		  })
		  .attr("metric_type", function(d) {
				return d.metric_type;
		  })
		  .on("mouseover", function(d) {
			  d3.select(this)     
				.style("cursor", "pointer")
				.append("text")
				.attr("class", "text")
				.text(`${d.metric_val}`)
				.attr("x", d => xScale(d.orginal_date) + 5)
				.attr("y", d => yScale(d.metrics_val) - 10);
			})
		  .on("mouseout", function(d) {
			  d3.select(this)
				.style("cursor", "none")  
				.transition()
				.duration(duration)
				.selectAll(".text").remove();
			})
		  .append("circle")
		  .attr("cx", d => xScale(d.orginal_date))
		  .attr("cy", d => yScale(d.metrics_val))
		  .attr("r", function(d,i){
			  	//console.log(d);
				 if(d.metric_type == 'actual')
					return circleRadius; 
				else if(d.metric_type == 'predicted' && d.pred_val==="1")
					return circleRadius_pred;  
				 
		  })
		  .style('opacity', circleOpacity)
		  .on("mouseover", function(d) {
				d3.select(this)
				  .transition()
				  .duration(duration)
				  .attr("r", function(d,i){
						
						if(d.metric_type == 'predicted')
							return circleRadiusHover_pred;  
						else
							return circleRadiusHover;  
				  });
			  })
			.on("mouseout", function(d) {
				d3.select(this) 
				  .transition()
				  .duration(duration)
				  .attr("r", function(d,i){
						
						if(d.metric_type == 'predicted'&& d.pred_val==="1")
							return circleRadius_pred;  
						else
							return circleRadius;  
				  });  
			  });
		
		
		/* Add Axis into SVG */
		var xAxis = d3.axisBottom(xScale).ticks(5);
		var yAxis = d3.axisLeft(yScale).ticks(5);
		
		svg.append("g")
		  .attr("class", "x axis")
		  .attr("transform", `translate(0, ${height-margin})`)
		  .call(xAxis)
		  .append('text')
		  .attr("x", (width-margin)/2)
		  .attr("y", 40)
		  .attr("transform", "rotate(0)")
		  .attr("fill", "#000")
		  .text("Hour");	
		
		svg.append("g")
		  .attr("class", "y axis")
		  .call(yAxis)
		  .append('text')
		  .attr("x", (margin-height)/2)
		  .attr("y", -30)
		  .attr("transform", "rotate(-90)")
		  .attr("fill", "#000")
		  .text("Metric value");	
	}
	
	return {
		init : init,
		render : render
	}	
};
