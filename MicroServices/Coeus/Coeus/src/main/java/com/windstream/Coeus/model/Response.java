package com.windstream.Coeus.model;

public class Response<T> {

	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", executionTime=" + executionTime + ", data="
				+ data + "]";
	}

	private boolean status;
	private String message;
	private int executionTime;
	private T data;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(int executionTime) {
		this.executionTime = executionTime;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	

}
