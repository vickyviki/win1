package com.windstream.Coeus.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.windstream.Coeus.dao.CoeusDAO;
import com.windstream.Coeus.model.Response;

@RestController
@EnableAutoConfiguration
public class Endpoint {
	
	@Autowired
	CoeusDAO coeusDAO;
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/modulelist", consumes = {
			MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public Response<List<String>> getModuleList() {
		
		Response<List<String>> response=new Response<List<String>>();
		response=returnSuccessResponse(response);
		response.setData(coeusDAO.getModule());
		
		return response;
		
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/filterbydatetime", consumes = {
			MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public Response<List<Map<String,Object>>> queryByTimestamp(@RequestBody Map<String, Object> payload) {
		
		Response<List<Map<String,Object>>> response=new Response<List<Map<String,Object>>>();
		response=returnSuccessResponse(response);
		response.setData(coeusDAO.queryByTimestamp(String.valueOf(payload.get("module")),String.valueOf(payload.get("from")), String.valueOf(payload.get("to"))));
		
		return response;
		
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/storedata", consumes = {
			MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public Response<Boolean> storeData(@RequestBody Map<String, Object> payload) {

		Response<Boolean> response = new Response<Boolean>();
		response = returnSuccessResponse(response);
		response.setData(
				coeusDAO.storeData(String.valueOf(payload.get("nodeid")), String.valueOf(payload.get("section")),
						String.valueOf(payload.get("module")), String.valueOf(payload.get("ts")),
						String.valueOf(payload.get("measure")), String.valueOf(payload.get("type")), Integer.valueOf(""+payload.get("f_val"))));

		return response;

	}
	
	private Response returnErrorResponse(final String errorMsg, final Response response) {

		response.setStatus(false);
		response.setMessage(errorMsg);
		return response;

	}
	
	private Response returnSuccessResponse(Response response) {

		response.setStatus(true);
		return response;

	}

}
