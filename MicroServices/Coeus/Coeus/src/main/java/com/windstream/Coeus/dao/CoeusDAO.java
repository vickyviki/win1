package com.windstream.Coeus.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CoeusDAO {

	@Value("${driver}")
	private String driver;

	@Value("${url}")
	private String url;

	@Value("${username}")
	private String username;

	@Value("${password}")
	private String password;

	private Connection getConnection() {
		long startTime=System.currentTimeMillis();
		Connection con = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(
					"jdbc:hive2://192.168.26.243:10000",
					"hive", "hive");
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("Time Consumption for Obtained Connection :: "+TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-startTime));

		return con;
	}

	public List<String> getModule() {
		long startTime=System.currentTimeMillis();
		Statement stmt;
		ResultSet res;
		List<String> moduleList = new ArrayList<String>();
		Connection con = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			stmt.execute("use coeus");
			res = stmt.executeQuery("select distinct module from coeus.ml_performance_metrics_prestaging LIMIT 25");
			while (res.next()) {
				moduleList.add(res.getString(1).trim());
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		System.out.println("Time Consumption for Obtained distinct module list :: "+TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-startTime));
		return moduleList;

	}

	public List<Map<String, Object>> queryByTimestamp(String module, String from, String to) {
		long startTime=System.currentTimeMillis();
		PreparedStatement stmt;
		ResultSet res;
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		Connection con = null;
		String query="select module,ts,measure,val,type,predicted_categorical,feedback_value_categorical from coeus.ml_performance_metrics_prestaging where trim(module)='MODULE_NAME' and ts > FROM_DATE and ts < TO_DATE order by ts".replaceAll("FROM_DATE", "'"+from+"'").replaceAll("TO_DATE", "'"+to+"'").replaceAll("MODULE_NAME", module);
		try {
			con = getConnection();
			stmt = con.prepareStatement(query
					//"select module,ts,measure,val,type,predicted_categorical from coeus.ml_performance_metrics_prestaging LIMIT 5");
			);
			//stmt.setString(1, module);
			System.out.println("From :: "+from);
			System.out.println("To :: "+to);
			System.out.println("Query ::"+query);
			//stmt.setDate(1, x);
			//stmt.setDate(2, x);
			//stmt.setString(1, from);
			//stmt.setString(2, to);
			res = stmt.executeQuery();
			while (res.next()) {
				Map<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("module", trimData(res.getString(1)));
				dataMap.put("timestamp", trimData(res.getString(2)));
				dataMap.put("measure", trimData(res.getString(3)));
				dataMap.put("value", trimData(res.getString(4)));
				dataMap.put("type", trimData(res.getString(5)));
				dataMap.put("predicted_categorical",res.getInt(6));
				dataMap.put("feedback_value_categorical",res.getInt(7));

				dataList.add(dataMap);
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		System.out.println("Time Consumption for QueryByTimestamp & Module :: "+TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-startTime));
		return dataList;

	}

	public boolean storeData(String nodeId, String section, String module, String ts, String measure, String type,int f_val) {
		long startTime=System.currentTimeMillis();
		PreparedStatement stmt;
		Connection con = null;
		boolean status=false;

		try {
			con = getConnection();
			stmt = con.prepareStatement(
					"insert into table coeus.ml_performance_metrics_prestaging(nodeid, section,module, ts, measure, type, feedback_flg,feedback_value_categorical) values (?,?,?,?,?,?,?,?)");
			stmt.setString(1, nodeId);
			stmt.setString(2, section);
			stmt.setString(3, module);
			stmt.setString(4, ts);
			stmt.setString(5, measure);
			stmt.setString(6, "user_feedback");
			stmt.setString(7, "Yes");
			stmt.setInt(8, f_val);
			stmt.executeUpdate();
			status= true;
		} catch (SQLException e) {
			status=false;
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		System.out.println("Time Consumption for insert :: "+TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-startTime));
		return status;
	}

	private String trimData(final String data) {
		if (data != null && !data.isEmpty()) {
			return data.trim();
		}
		return data;
	}

}
