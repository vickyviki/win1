package com.windstream.coeus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoeusApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoeusApplication.class, args);
	}
}

