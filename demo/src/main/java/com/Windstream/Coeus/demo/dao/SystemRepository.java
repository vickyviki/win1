package com.Windstream.Coeus.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.Windstream.Coeus.demo.Model.System;
 
@Repository
public interface SystemRepository extends CrudRepository<System,Long> {
     
 
}